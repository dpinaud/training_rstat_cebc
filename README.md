# AppliedStats_R_CEBC

Training course “Statistics applied to Ecology with R” in CEBC UMR7372 CNRS / La Rochelle University  
David Pinaud (pinaud@cebc.cnrs.fr)  
2024-03-05  


The training "Applied statistics with R: Statistical modelling" is for people with an intermediate level, with some *prerequisites*: 
- some autonomy with R (import a file, manage some R objects like data.frames...)
- some knowledge for basic stats (what is a statistical test, a distribution, a sample...).  

\
It is based on materials:  
- [the slides](https://mycore.core-cloud.net/index.php/s/2DFs7J4RPGFrTg6)  
- [the R scripts and datasets](https://mycore.core-cloud.net/index.php/s/pzOl3UKHrZxaniZ)  

\
The program:
- Introduction and some reminders of statistics
- probability laws and random draws
- statistical models with R: the linear model
- non-linear models (GAM and NLS)
- Non-normality of residuals: generalized linear models (GLM)
- Selection of models by AIC
- Dealing with the heterogeneity of the variance with the GLS 
- Dealing with the non-independence of the residuals with the mixed models and the GLS, spatial autocorrelation
- Multivariate (PCA) and discriminant analysis, classifications.
