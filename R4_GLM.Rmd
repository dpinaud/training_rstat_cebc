---
title: "R4: the Generalized Linear Model"
author: "David Pinaud"
date: "2024-02-19"
output: 
  html_document:
    toc: true
    toc_depth: 2
    number_sections: true
    toc_float: 
       collapsed: false
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Training course “Statistics applied to Ecology with R” in CEBC - CNRS / La Rochelle University  
David Pinaud (pinaud@cebc.cnrs.fr)  
\
2024-02-09, modified `r round(as.POSIXct(Sys.time()))`.  
Built with R `r getRversion()`.\
\
Download R scripts and datasets [here](https://mycore.core-cloud.net/index.php/s/pzOl3UKHrZxaniZ)
\
\
\
\
\
\
\
 
**Non-Gaussian distribution of residuals: the Generalized Linear Model (aka GLM)**  

Generalization of linear models for fitting other (non-Gaussian) distributions.  

\
\
\
\


# Example with a Poisson distribution  
Expected when you have count data.  
\
We use a dataset that gives the number of prize for students according to their teaching program and their math achievement. As usual, we explore first the data and then fit a linear model and try to improve it.

```{r}
library(ggplot2)
library(DHARMa)

p <- read.csv("https://mycore.core-cloud.net/index.php/s/kQq6CSlXFmUm9Tr/download")  # poisson_sim.csv
p$prog <- factor(p$prog, levels=1:3, labels=c("General", "Academic", "Vocational"))  #"prog" as a factor

hist(p$num_awards, col="blue")
coplot(num_awards ~ math | prog, data=p)

ggplot(p, aes(math, num_awards, col=prog)) +
	geom_point() + facet_wrap(~prog)

m0 <- lm(num_awards ~ prog + math, data=p)
summary(m0)
op <- par(mfrow=c(2,2))
plot(m0)
par(op)
simulationOutput <- simulateResiduals(fittedModel = m0, plot = F)
plot(simulationOutput) # left: Q-Q plot ; right: residuals against predicted values + quantiles regression
## Can we trust these parameters estimations??

m1 <- glm(num_awards ~ prog + math, family="poisson", data=p)
summary(m1)
op <- par(mfrow=c(2,2))
plot(m1)
par(op)
simulationOutput <- simulateResiduals(fittedModel = m1, plot = F)
plot(simulationOutput) # left: Q-Q plot ; right: residuals against predicted values + quantiles regression

# test for dispersion 
deviance(m1) / df.residual(m1)  # usually close to 1 for Poisson distribution, here 0.96
testDispersion(m1) 

# test of goodness of fit for the expected distribution
1 - pchisq(deviance(m1), df.residual(m1))  # small values (< 0.05) = lack of fit
testUniformity(m1)
```
\
\

# An example of overdispersion  

Overdispersion occurs when the variance is larger than expected.  
For a Poisson distribution, we have  
$\sigma = \mu$,  
so  
$\frac {\sigma}{\mu} = k \approx 1$.  
$k$ is known as the dispersion parameter.  
\
We will use a dataset that gives the number of cancers in population clusters as a function to the distance to a pollutant source. After exploring the data, we start first by a linear model.  


```{r}
clust <- read.table("https://mycore.core-cloud.net/index.php/s/WeZW4GbzrjZ6WsH/download", sep=",", h=T)
str(clust)

hist(clust$Cancers, col="green")
hist(clust$Distance, col="green")

plot(Cancers ~ Distance, data=clust)

lm1 <- lm(Cancers ~ Distance, data=clust)
op <- par(mfrow=c(2,2))
plot(lm1)
par(op)
hist(resid(lm1))
summary(lm1)
simulationOutput <- simulateResiduals(fittedModel = lm1, plot = F)
plot(simulationOutput) 
```

Not convincing.  
We adjust a Poisson distribution.  

```{r}
glm1 <- glm(Cancers ~ Distance, data=clust, family = poisson())
op <- par(mfrow=c(2,2))
plot(glm1)
par(op)
simulationOutput <- simulateResiduals(fittedModel = glm1, plot = F)
plot(simulationOutput) 

# test of goodness of fit for the expected distribution
1 - pchisq(deviance(glm1), df.residual(glm1))  # small values (< 0.05) = lack of fit
testUniformity(m1)

# test for dispersion 
deviance(glm1) / df.residual(glm1)  # usually close to 1 for Poisson distribution, here 0.96
testDispersion(m1) 

simulationOutput <- simulateResiduals(fittedModel = glm1, plot = F)
testZeroInflation(simulationOutput) # it seems an excess of 0s than expected (Zero-Inflated distribution)

summary(glm1)
```

Not convincing, overdispersion!  
We adjust a quasiPoisson distribution, that allows flexibility to deal with overdispersion.    

```{r}
glm2 <- glm(Cancers ~ Distance, data=clust, family = quasipoisson)
op <- par(mfrow=c(2,2))
plot(glm2)
par(op)
# dispersion
deviance(glm2) / df.residual(glm2)

# goodness of fit 
1 - pchisq(deviance(glm2), df.residual(glm2))  # small values (< 0.05) = lack of fit
summary(glm2)
```

Hum, overdispersion still present...  
We adjust a negative binomial distribution that allows an extra parameter to deal explicitely with overdispersion.    
```{r}

library(MASS)
glm3 <- glm.nb(Cancers ~ Distance, data=clust)
deviance(glm3) / df.residual(glm3)
1 - pchisq(deviance(glm3), df.residual(glm3))
summary(glm3)

```
\

# An example with 0/1 data (logistic model, Binomial distribution)  

The dataset represents the number of White-crowned Sparrow detected during 5-min count points in the boreal forest. 
`BRGB`gives the number of detected sparrows corrected for the probability of detection (`NA` indicates no sparrow detected).  
Sampling points were located in exploited forest stands within a panel of varying age (time since the last cut), ranging from 5 to 75 years. Some stands were never cut (`VIN` class).  
We hypothesyze that the age of the forest affects the density of sparrows.  

## Data exploration and modelisation  

```{r}
af <- read.table("https://mycore.core-cloud.net/index.php/s/yNHcIIsRDqC3kaw/download", h=T, sep="\t")
str(af)
head(af)

hist(af$BRGB, col="green")
plot(BRGB ~ Age, data=af)  # It seems that the information is roughly contained in a presence / absence distribution...

af$brgb01 <- ifelse(is.na(af$BRGB), 0, 1) # we create a variable accordingly
table(af$brgb01)

          # We work only with cut stands
afc <- af[af$Class != "VIN", ]

    # logistic model
maf <- glm(brgb01 ~ Age, data=afc, family=binomial)  # linear effect of age
# summary(maf)

        # a quadratic effect of age?
maf2 <- glm(brgb01 ~ I(Age^2) + Age, data=afc, family=binomial) 
# summary(maf2)

        # comparaison of both models 
        # (relevant only if the datasets are strictly the same, beware of NAs!!)
anova(maf, maf2, test="Chisq")
AIC(maf, maf2)
BIC(maf, maf2)


		# it is interesting to add a null model in the comparison (y = cst):
m0 <- glm(brgb01 ~ 1, data=afc, family=binomial)
anova(m0, maf, maf2, test="Chisq")
AIC(m0, maf, maf2)

		# We can present the AIC table in a better way:
library(AICcmodavg)
Cand.models <- list()
Cand.models[[1]] <- m0
Cand.models[[2]] <- maf
Cand.models[[3]] <- maf2
Modnames <- lapply(Cand.models, "formula")  # extract the formula for each model
knitr:::kable(aictab(cand.set = Cand.models, modnames = paste0(Modnames), sort = TRUE))
knitr:::kable(bictab(cand.set = Cand.models, modnames = paste0(Modnames), sort = TRUE))
    # we can calculate the "evidence ratio" that can be useful to quantify the amount 
    # of support in favor of a model relative to a competing model (Burnham and Anderson, 2002):
at <- aictab(cand.set = Cand.models, modnames = paste0(Modnames), sort = TRUE)
evidence(aic.table=at)
bt <- bictab(cand.set = Cand.models, modnames = paste0(Modnames), sort = TRUE)
evidence(bt)

```

We see that the model with the lower AICc is the quadratic one, but this model is also equivalent to the simple one ($\Delta$AICc < 2).  
The model with the lower BIC is the simple one, with an equivalence the quadratic one ($\Delta$BIC < 2).  
Adopting the principle of parcimony, we retain the model with the smallest number of parameters (i. e. brgb01 ~ Age, with k=2). According to BIC, there is an evicence that the linear model is 2.6 times more parcimonious than the quadratic one.

## Plotting the model  

Now we represent the data with the final model.

```{r}
plot(brgb01 ~ Age, data=afc) ## ugly
sunflowerplot(afc$Age, afc$brgb01)
       
table(afc$brgb01, afc$Age)   # useful data
xy.tab <- xyTable(afc$brgb01, afc$Age)   # useful for plotting

plot(brgb01 ~ Age, data=afc, type="n", xlim=c(0, 65), ylim=c(-0.1, 1.1), xlab="Time since the last cut (Stand age, years)", ylab="Probability of presence", main="White-throated sparrow in the boreal forest")
symbols(xy.tab$y, xy.tab$x, circles=xy.tab$number, inches=F, add=T)

plot(brgb01 ~ Age, data=afc, type="n", xlim=c(0, 65), ylim=c(-0.1, 1.1), xlab="Time since the last cut (Stand age, years)", ylab="Probability of presence", main="White-throated sparrow in the boreal forest")
symbols(xy.tab$y, xy.tab$x, circles=xy.tab$n/8, inches=F, add=T, bg="green")  # we divide the radius by an integer to reduce the size of the circles

     # adding the model
nd <- seq(0, 65, 0.5) # sequence of X where the prediction will occur
pred <- predict(maf, newdata=list(Age=nd), type="response", se.fit=T)  # prediction of the final model on 

lines(nd, pred$fit, lwd=2)  # add the model
          # We want to add the confidence interval of prediction at 95%. CI = SE * t-value(ddf)
summary(maf)  # ddf ? 
maf$df.residual  # = df.residual(maf)
# and t-value?
qt(0.975, df.residual(maf))           
lines(nd, pred$fit-pred$se.fit*qt(0.975, df.residual(maf)), lty=2)   # CI95% = SE*qt(0.975, df)
lines(nd, pred$fit+pred$se.fit*qt(0.975, df.residual(maf)), lty=2)

		# what is the "native" probability of presence? (for uncut/VIN forest?)
tt <- table(af[af$Class == "VIN", "brgb01"])
abline(h=tt[2]/sum(tt), lty=2, col="grey")

```



